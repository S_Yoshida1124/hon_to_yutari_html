// JavaScript Document
jQuery(window).load(function() {

	//該当のセレクタなどを代入

	var mainArea = jQuery("#wrapper");//メインコンテンツ
	var sideWrap = jQuery("#sidebar");//サイドバー外枠
	var sideArea = jQuery(".sidebar_contents");//サイドバー

	/*設定ここまで*/

	var wd = jQuery(window);//ウィンドウ自体

	//メインとサイドの高さを比べる

	var mainH = mainArea.height();
	var sideH = sideWrap.height();

	if(sideH < mainH){  //メインの方が高ければ色々処理する

		//サイドバーの外枠をメインと同じ高さにしてrelaltiveに（.sidebar_contentsをポジションで上や下に固定するため）
		sideWrap.css({"height": mainH,"position": "relative"});

		//スクロールした時にサイドバーがウィンドウよりどれぐらいの高さではみ出してるか
		var sideOver = wd.height()-sideArea.height();

		//スクロールした時に固定を開始する位置 = サイドバーの座標＋はみ出す距離
		var fixPoint = sideArea.offset().top + (-sideOver);

		//固定を解除する位置 = メインコンテンツの終点
		var breakPoint = sideArea.offset().top + mainH;

		//スクロール中の処理
	wd.scroll(function(){


				//サイドメニューが画面より大きい場合
				if(wd.height() < sideArea.height()){
					if(fixPoint < wd.scrollTop() && wd.scrollTop() + wd.height() < breakPoint){//固定範囲内
					sideArea.css({"position" : "fixed" , "bottom" : "20px"});

					}else if(wd.scrolltop() + wd.height() >= breakPoint){ ////固定解除位置を超えた時

					}else { //その他上に戻った場合
						sideArea.css("position" , "static" );
					}

					}else {//サイドメニューが画面より小さい場合
						var sideBtm = wd.scrollTop() + sideArea.height(); //サイドメニューの終点
						if (mainArea.offset().top < wd.scrollTop() && sideBtm < breakPoint) { //固定範囲内
							sideArea.css({"position" : "fixed" ,"top" : "20px"});
					
						}else if (sideBtm >= breakPoint) { //固定解除位置を超えた時
					
							//サイドバー固定場所(bottomを指定すると不具合が出るのでtopから固定位置を算出する
							var fixedSide = mainH - sideH;
							sideArea.css({"position" : "fixed" , "top" : fixedSide});
					
						}else {
							sideArea.css("position" , "static");
						}
					}
		});
	}
});