//PAGETOP
	$(document).ready(function() {
		$('#page_top a').smoothScroll();
	});
	

//fixheight

jQuery(document).ready(function(){
	$(".fixHeight").fixHeight();
});


//プレイスホルダーIE8対応

$(function()
{
    $('.shop_search').ahPlaceholder({
          placeholderColor : '#948472',
          likeApple : false
     });
});
$(function()
{
    $('.search_keyword').ahPlaceholder({
          placeholderColor : '#231815',
          likeApple : false
     });
});
$(function()
{
    $('.contact_form').ahPlaceholder({
          placeholderColor : '#000000',
          likeApple : false
     });
});


//toggle
/*$(function(){
	$("#category_menu .ttl").on("click", function() {
		$(this).next().slideToggle();
		$(this).toggleClass("active");     //追加した一文
	});
});*/

//TOKINO accordion menu
	$(function(){

		//オブジェクトを保存
		var accordionItem=$('#category_menu');
		//一旦全部消す
		accordionItem.find('div').hide();

		//active要素を指定して開く
		var no=0;
		//accordionItem.find('h3').eq(no).addClass('active').next('div').show();

		//click-action
		accordionItem.find('h3').click(function () {

			//slide
			$(this).next('div').slideToggle("fast")
			.siblings('div:visible').slideUp("fast");
			//activeクラス切り替え
			$(this).toggleClass('active');
			$(this).siblings('h3').removeClass('active');

		});

		//hover-toggle
		accordionItem.find('h3').hover(function () {

			//toggle hoveredクラス
			$(this).toggleClass('hovered');

		});
	});



//ラジオボタン・チェックボックス・セレクトボックスをオリジナルに

$(function () {
	$('.fmselect').customSelect();
});


//トップスライダー

$(window).load(function() {
	$('#slider_01').flexslider({
		animation: "slide",
		move: 1,
		controlNav: false,
	  });
	});


