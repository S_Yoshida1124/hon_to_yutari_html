function initialize() {

var infoWindow = new google.maps.InfoWindow();
var mapOptions = {
  zoom: zm,
  center: myLatlng,
  mapTypeId: google.maps.MapTypeId.ROADMAP
};
var map = new google.maps.Map(document.getElementById("map_canvas"),
    mapOptions);

/*アイコン*/
var icon = new google.maps.MarkerImage("http://hontoyutari.com/google/icon.png",/*アイコンのPath*/
new google.maps.Size(22,40),/*アイコンサイズ*/
new google.maps.Point(0,0)/*位置調整*/
);

/*スタイルのカスタマイズ*/
var styleOptions =
[
{
"featureType": "landscape.natural",
"stylers": [
{ "color": "#e0e0e0" }
]
},{
"featureType": "transit.line",
"stylers": [
{ "invert_lightness": true },
{ "visibility": "simplified" },
{ "color": "#675d52" }
]
},{
"featureType": "landscape.man_made",
"elementType": "geometry",
"stylers": [
{ "visibility": "simplified" },
{ "color": "#c2c2c2" }
]
},{
"featureType": "poi",
"elementType": "geometry",
"stylers": [
{ "color": "#aad48f" }
]
},{
"featureType": "water",
"stylers": [
{ "color": "#dfe8ff" }
]
},{
"featureType": "transit.station",
"elementType": "geometry",
"stylers": [
{ "color": "#ae906e" }
]
}
];
var styledMapOptions = { name: "本とゆたり" }
var thismapType = new google.maps.StyledMapType(styleOptions, styledMapOptions);
map.mapTypes.set("thismap", thismapType);
map.setMapTypeId("thismap");

/*マーカーの設置*/
var markerOptions = {
position: myLatlng,/*表示場所と同じ位置に設置*/
map: map,
icon: icon,
title: name
};
var marker = new google.maps.Marker(markerOptions);
/*情報ウインドウ*/
if(hpurl == ''){
		var html = "<div id='maplist'><div class='inf clearfix'>"
		+ name
	}else{
		var html = "<div id='maplist'><div class='inf clearfix'><a href='"+ hpurl +"' target=_blank>"
		+ name +"</a>"
	}
	html += "<br />"+ adres + "</div>"
	+ "</div>";
google.maps.event.addListener(marker, 'click', function() {
		infoWindow.setContent(html); //情報ウィンドウの内容
		infoWindow.open(map,marker); //情報ウィンドウを表示
	});

}